#!/bin/bash

set -euo nounset

readonly PACKAGE_FILE="$1"
readonly DEP_NAME="$2"
readonly DEP_TYPE="$3"
readonly CURRENT_DEP_VERSION="$4"
readonly NEW_DEP_VERSION="$5"

echo "$PACKAGE_FILE, $DEP_NAME, $DEP_TYPE, $CURRENT_DEP_VERSION, $NEW_DEP_VERSION"

readonly REPO_ROOT="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

readonly FILE_NAME="$(basename "$PACKAGE_FILE")"
readonly CONTAINER_NAME="$(basename "$(dirname "$PACKAGE_FILE")")"
readonly CONTAINER_TYPE="$(basename "$(dirname "$(dirname "$PACKAGE_FILE")")")"
readonly VERSION_FILE_PATH="${REPO_ROOT}/${CONTAINER_TYPE}/${CONTAINER_NAME}/VERSION"

if [[ "$CONTAINER_TYPE" == "apps" ]]; then

    readonly CURRENT_VERSION="$(cat "$VERSION_FILE_PATH" | awk '{print $1}')"
    readonly MAJOR="$(echo "$CURRENT_VERSION" | cut -d. -f1)"
    readonly MINOR="$(echo "$CURRENT_VERSION" | cut -d. -f2)"
    readonly PATCH="$(echo "$CURRENT_VERSION" | cut -d. -f3)"

    readonly NEW_PATCH="$(expr "$PATCH" + 1)"

    echo "Bumping ${REPO_ROOT}/apps/${CONTAINER_NAME} to ${MAJOR}.${MINOR}.${NEW_PATCH}"

    echo "${MAJOR}.${MINOR}.${NEW_PATCH}" > "$VERSION_FILE_PATH"

elif [[ "$CONTAINER_TYPE" == "mirrors" ]]; then

    echo "Using ${DEP_TYPE}:${DEP_NAME} version"

    echo "${NEW_DEP_VERSION}" > "$VERSION_FILE_PATH"
fi

